require 'forum2discourse/models/dday'

# Each exporter should return a collection of
# Forum2Discourse::Discourse::Topic, each of which have many
# Forum2Discourse::Discourse::Post associated with them. Each topic and post
# should be associated with a Forum2Discourse::Discourse::User

module Forum2Discourse::Exporters
  class DDay
    Forum2Discourse::Exporter.register(:dday, self)

    def initialize(options)
      self
    end

    def topics(args={})
      @topics ||= convert(Forum2Discourse::Models::DDay::Topic.all(args))
    end

    private

    def convert(collection)
      collection.map(&:to_discourse).compact
    end
  end
end
