class Forum2Discourse::Models::DDay::User
  include DataMapper::Resource

  storage_names[:default] = "eve_user"

  property :id,       Serial
  property :user,    String
  property :password, String, :field => 'pass'
  property :title,    String
  property :first_name, String
  property :last_name, String
  

  def to_discourse

    Forum2Discourse::Models::Discourse::User.new({
      username: title,
      email: user,
      name: "#{first_name} #{last_name}"
    })
  end
end

