class Forum2Discourse::Models::DDay::Post
  include DataMapper::Resource

  storage_names[:default] = "eve_forum"

  property :id,         Serial
  property :poster,     String, :field => 'author_string'
  property :poster_id,  Integer, :field => 'author'
  property :message,    Text, :field => 'body'
  property :posted,     DateTime, :field => 'created'
  property :topic_id,   Integer, :field => 'tree'

  belongs_to :topic, 'Forum2Discourse::Models::DDay::Topic'
  belongs_to :user, 'Forum2Discourse::Models::DDay::User', child_key: [ :poster_id ]

  def text_parser(msg)
    # msg.gsub!(/\r/," ")
    # msg.gsub!(/\n/,"")

    msg.gsub!(/\[(.+?)\]/, &:downcase)
    msg.gsub!(/(\[xs\].*?\[\/xs\])/, "")
    return msg
  end

  def to_discourse
    poster_email = "example.com"
    duser = if user
              user.to_discourse
            else
              email = (poster_email.nil? || poster_email.empty?) ? "#{poster}.no.email@example.com.invalid" : poster_email
              Forum2Discourse::Models::Discourse::User.new({username: poster, email: email, name: poster})
            end
    
    duser = duser.valid? ? duser : Forum2Discourse::Models::Discourse::User.anonymous
  
    Forum2Discourse::Models::Discourse::Post.new({
      title: '',
      category: topic.forum_name,
      user: duser,
      raw: text_parser(message),
      created_at: posted
    })
  end
end
