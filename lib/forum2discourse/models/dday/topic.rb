class Forum2Discourse::Models::DDay::Topic
  include DataMapper::Resource

  CATEGORIES = { 1 => "PC",
                 76 => "TV",
                 77 => "TV",
                 82 => "Hifi e Home Theater",
                 83 => 'Hifi e Home Theater',
                 84 => 'Hifi e Home Theater',
                 1125 => 'Hifi e Home Theater',
                 75 => 'Hifi e Home Theater',
                 89 => 'TV',
                 79 => 'Fotografia',
                 91 => 'Fotografia',
                 85 => 'Hifi e Home Theater',
                 90 => 'Smartphone',
                 81 => 'Gadget & Wearable',
                 1388 => 'Tablet',
                 1473 => 'Gadget & Wearable',
                 78 => 'PC',
                 74 => 'PC',
                 87 => 'PC',
                 88 => 'PC',
                 86 => 'PC',
                 1305 => 'PC',
                 80 => 'Gaming'
                }

  storage_names[:default] = "eve_forum"

  property :id,           Serial
  property :poster,       String, :field => 'author_string'
  property :subject,      Text, :field => 'title'
  property :posted,       DateTime, :field => 'created'
  property :num_views,    Integer, :field => 'view'
  property :forum_id,     Integer, :field => 'category'
  property :forum_type,     Integer, :field => 'type'
  property :main,     Integer, :field => 'level'

  has n, :posts, 'Forum2Discourse::Models::DDay::Post'
  # belongs_to :forum, 'Forum2Discourse::Models::DDay::Forum'

  def forum_name
    if self.forum_type == 1
      return "Supporto"
    elsif self.forum_type == 2
      return "Relax"
    elsif self.forum_type == 4
      return "Arena DDay"
    elsif self.forum_type == 5
      return "App e Software"
    else
      return CATEGORIES[self.forum_id]
    end
  end

  def to_discourse
    # Break early if the forum this post should be in does not exist anymore
    return nil if main != 0
    Forum2Discourse::Models::Discourse::Topic.new({
      id: id,
      title: subject,
      created_at: posted,
      category: forum_name,
      views: num_views,
      posts: posts.map(&:to_discourse)
    })
  end
end
